/* AUTOR: Ibai Marcos Cincunegui  
 * NIA: 710488 
 * FICHERO: FTPService.java  
 * VERSION: 1.0
 * TIEMPO: 2h
 * DESCRIPCION: Clase abstracta que contiene los comandos/respuestas que soporta el servicio,
 * tanto del cliente como del servidor, así como los metodos asociados a dichos comandos 
 * como transmitir un datagrama UDP, obtener comando de datagrama...
 * 
 */

package ftpService;


import java.io.IOException;
import java.net.*;


public class FTPService {
	  public static final int SIZEMAX = 255; // Maximum size of datagram
	  public static final int SERVERPORT = 5000; // default server port

	  static public enum Command {HELLO, LIST, GET, QUIT, ERROR};
	  static public enum Response {WCOME, OK, PORT, SERVERROR,BYE,UNKNOWN}; 
	  
	  FTPService(){
		  
	  }
	  
	  /**
	   * Metodo que obtiene el comando que se solicita en el datagrama UDP.
	   * Se pasa un string que contiene el campo de datos del datagrama, y 
	   * devuelve el comando que contiene. Si no se reconoce ningun comando,
	   * devuelve ERROR.
	   * @param textcommand
	   * 	Comando de tipo String
	   * @return
	   * 	Comando de tipo Command
	   */
	  public static Command commandFromString(String textcommand){
		  Command comando = null;
		  
		  if(textcommand.indexOf("HELLO")!=(-1)){
			  comando = Command.HELLO;
		  }else if(textcommand.indexOf("LIST")!=(-1)){
			  comando = Command.LIST;
		  }else if(textcommand.indexOf("GET")!=(-1)){
			  comando = Command.GET;
		  }else if(textcommand.indexOf("QUIT")!=(-1)){
			  comando = Command.QUIT;
		  }else{
			  comando = Command.ERROR;
		  }	  
		  		  
		  return comando;
	  }
	  
	  
	  /**
	   * Metodo que obtiene la respuesta que se solicita en el datagrama UDP.
	   * Se pasa un string que contiene el campo de datos del datagrama, y 
	   * devuelve la respuesta que contiene. Si no se reconoce ninguna respuesta,
	   * devuelve UNKNOWN.
	   * @param textcommand
	   * 	Respuesta de tipo String
	   * @return
	   * 	Respuesta de tipo Response 
	   */
	  public static Response responseFromString(String textresponse){
		
		Response comando = null;
		  
		  if(textresponse.indexOf("Welcome")!=(-1)){
			  comando = Response.WCOME;
		  }else if(textresponse.indexOf("OK")!=(-1)){
			  comando = Response.OK;
		  }else if(textresponse.indexOf("PORT")!=(-1)){
			  comando = Response.PORT;
		  }else if(textresponse.indexOf("SERVERROR")!=(-1)){
			  comando = Response.SERVERROR;
		  }else if(textresponse.indexOf("BYE")!=(-1)){
			  comando = Response.BYE;
		  }	else{
			  comando = Response.UNKNOWN;
		  }
		  
		  return comando;
	  }
		  		
	  
	  /**Metodo que devuelve un string el nombre completo del fichero que se 
	   * pide en un comando GET.
	   * Se espera un formato del payload UDP:
	   * 	GET file.dat	   * 
	   * @param textcommand
	   * 	Payload del datagrama UDP que contiene el fichero solicitado
	   * @return
	   * 	El nombre completo del fichero
	   */
	  public static String requestedFile(String textcommand){
		  
		  	int i ;
		  	String value;
		  	i=textcommand.indexOf(" ");
		  	value = textcommand.substring(i+1, textcommand.length());
		  	
		  	return value; 
		  
	  }
	  
	  /** Metodo que obtiene el puerto que se indica en una respuesta PORT
	   * a partir del payload del mensaje UDP de tipo String.
	   * 
	   * @param textresponse
	   * 	Pauload del datagrama que contiene el mensaje de tipo String
	   * 	en formato: PORT 21
	   * @return
	   * 	El puerto como entero. 	
	   */
	  public static int portFromResponse(String textresponse){
		  int i,value;
			
		  	i=textresponse.indexOf(" ");
		  	value = Integer.parseInt(textresponse.substring(i+1));
		  	
		  	return value;
	  }

	  
	  /**
	   * Metodo que transmite un paquete UDP
	   * @param socket
	   * 	Socket UDP por el que se transmite el Datagrama
	   * @param text
	   * 	Payload de mensaje.
	   * @param destPort
	   * 	Puerto destino del datagrama
	   * @param destAddr
	   * 	Direccion destino del datagrama
	   */
	  public static void sendUDPmessage(DatagramSocket socket, String text, int destPort, InetAddress destAddr) throws IOException{
		  	byte []buffer = new byte[SIZEMAX];
			DatagramPacket sendPacket = new DatagramPacket(buffer,SIZEMAX);
							
			sendPacket.setData(text.getBytes(),0,text.length());
			sendPacket.setPort(destPort);
			sendPacket.setAddress(destAddr);		
			socket.send(sendPacket);
	  }  
	  


}
