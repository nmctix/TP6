package cliente;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

import ftpService.FTPService;

public class MultiFTPClient {

	private final int SIZEPACKET = 255;	
	public static String serverFileName = "file.txt";
	private static Scanner bufferTeclado = new Scanner(System.in);
	public InetAddress server1;
	public InetAddress server2;
	public InetAddress server3;
	public InetAddress server4;
	public int port1;
	public int port2;
	public int port3;
	public int port4;
	public boolean serverStatus1;
	public boolean serverStatus2;
	public boolean serverStatus3;
	public boolean serverStatus4;
	
	public DatagramSocket sockUDP;
	
	public static void main(String[] args) {
		boolean salir = false;
		int aviableServers;
		
		aviableServers = verificarServidores(serverFileName);
		while(!salir){
			System.out.println("Introducir comando: ");
			String datos = bufferTeclado.nextLine();
			
			// list();
			// get();
			// quit();
			
			
		}
		
	}


	/** Lee el fichero fileName y obtiene las direcciones, puertos y anchos de banda de cada servidor.
	 * Transmite un client Hello y identifica el estado de cada servidor obteniendo las respuestas.
	 * Devuelve la cantidad de servidores activos.
	 * 
	 * @param serverFileName2 fichero de informacion de servidores
	 * @return numero de servidores disponibles
	 */
	
	private static int verificarServidores(String fileName) {
		
		
		// TODO Auto-generated method stub
		return 0;
		
	}
	
	
	/**
	 * Metodo que realiza de forma concurrente, un LIST a todos los servidores,
	 * y muestra por pantalla el conjunto de todos los ficheros disponibles entre
	 * todos los servidores (no repetidos).
	 */
	private static void list(){
		
	}
	
	/**
	 * 
	 * Metodo que realiza de forma concurrente un GET de un fichero disponible en alguno de los
	 * servidores. Se siguen estos pasos:
	 * @1: En funcion del tamaño del fichero, del numero de servidores que disponen el fichero
	 * y del ancho de banda de cada uno, se calcula el tamaño de la particion del fichero que
	 * se va a solicitar a cada servidor.
	 * @2: Se realiza un hilo por cada conexion, y en cada hilo se realiza un PORT al correspondiente
	 * servidor para iniciar la conexion TCP.
	 * @3: Se genera un nuevo fichero que contiene el conjunto de todos los ficheros obtenidos.
	 */
	private static void get(){
		
	}
}
